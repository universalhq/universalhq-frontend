import React from 'react';
import { Link as RouterLink, Route, Switch } from 'react-router-dom';
import {Container, Link} from "@material-ui/core";

function App() {
  return (
    <Container>
      <nav>
        <ul>
          <li>
            <Link to="/" component={RouterLink}>Home</Link>
          </li>
          <li>
            <Link to="/about" component={RouterLink}>About</Link>
          </li>
          <li>
            <Link to="/users" component={RouterLink}>Users</Link>
          </li>
        </ul>
      </nav>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/about">
          <About/>
        </Route>
        <Route path="/users">

          <Users/>

        </Route>
        <Route path="/">

          <Home/>

        </Route>
      </Switch>
    </Container>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}


export default App;
