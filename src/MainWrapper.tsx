import React from 'react';

import theme from './theme';
import "@fontsource/roboto"
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store } from './store/store'

type MainWrapperProps = React.PropsWithChildren<{}>

/**
 * This is used to wrap component application and provide what's needed to have common elements between Storybook
 * and the application.
 *
 * Should provide
 * - Material UI theme
 * - Redux store
 * - Router
 *
 * @param children
 * @constructor
 */
function MainWrapper({ children }: MainWrapperProps) {
  return (
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Provider store={store}>
          <MemoryRouter>
            {children}
          </MemoryRouter>
        </Provider>
      </ThemeProvider>
    </React.StrictMode>
  )
}

export default MainWrapper;
