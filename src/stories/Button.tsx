import React from 'react';
import './button.scss';
import MaterialUiButton from '@material-ui/core/Button';

export enum Color {
  primary = 'primary',
  secondary = 'secondary'
}

export interface ButtonProps {
  color?: Color,
  disableElevation?: boolean
}

/**
 * This is a simple yet DUMMY example of storybook. It shows off a really little bit MaterialUI Icon component.
 * @param color
 * @param disableElevation
 * @param props
 * @constructor
 */
export const Button: React.FC<ButtonProps> = ({
                                                color,
                                                disableElevation,
                                                ...props
                                              }) =>
  (
    <MaterialUiButton {...props} color={color}  disableElevation={disableElevation} variant="contained">Default</MaterialUiButton>
  );
