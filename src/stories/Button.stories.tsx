import React from 'react';
import {Meta, Story} from '@storybook/react';

import {Button, ButtonProps, Color} from './Button';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
};


export const Primary = Template.bind({});
Primary.args = {
  color: Color.primary,
};

export const Secondary = Template.bind({});
Secondary.args = {
  color: Color.secondary,
};

export const DisableElevation = Template.bind({});
DisableElevation.args = {
  disableElevation: true,
  color: Color.secondary
};
