import { combineReducers, configureStore, Action, ThunkAction } from '@reduxjs/toolkit'

const globalHqReducer = combineReducers({})


export const store = configureStore({
  reducer: {
    exampleReducer: globalHqReducer,
  },
});


export type GlobalHQState = ReturnType<typeof globalHqReducer>

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>>
