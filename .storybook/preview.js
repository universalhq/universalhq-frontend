import MainWrapper from "../src/MainWrapper";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

export const decorators = [
  (Story) => (
    <MainWrapper>
      <Story />
    </MainWrapper>
  ),
]
