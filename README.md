# Universal HQ

> This project aims to replace Foxehole Global HQ for both teams of [Foxehole Game](https://www.foxholegame.com/)

[[_TOC_]]

# Technology stack

- [CRA](https://create-react-app.dev/docs/getting-started/)
- [react-storybook](https://storybook.js.org/docs/react/get-started/introduction)
- [react-router-dom](https://reactrouter.com/web/guides/quick-start)
- [redux-toolkit](https://redux-toolkit.js.org/)
- [material-ui](https://material-ui.com/)

# Development

- Node 15+

## Application
```bash
$ npm install

# Start the app
$ npm start
```

## Components

```bash
$ npm install

# Start storybook
$ npm run storybook
```

# Repository Structure

```
.
├── .storybook/                # Storybook wrapper / configuration files 
├── public/                    # Publicly accessible resources 
│   ├── icons                  # General icons, icons TAKEN FROM FOXEHOLE GAME with their permission 
├── src                        # React application files
│   ├── store/                 # Redux & Redux Toolkit related files                
│   ├── stories/               # Storybook files, put your isolated components there !                
│   ├── App.*                  # Main application entrypoint                
│   ├── index.tsx              # Standard CRA entrypoint                
│   └── MainWrapper.tsx        # Loads & Setup everything to run the App & components                
├── package.json               # Dependencies
├── README.md                  # This help file
└── tsconfig.json              # Typescript configuration
```

# Docs

- [How to maintain icons](./public/icons/README.md)
